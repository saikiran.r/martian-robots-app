import React from 'react';
import axios from 'axios';
import NewlineText from "./newLinetext";

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            inputValue: '',
            outputValue: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({inputValue: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        let {inputValue} = this.state;
        if(!inputValue){
            this.setState({outputValue: "Input is empty", invalid: true});
            return;
        }
        console.log(inputValue);
        let headers = {"headers": {"content-type": "application/json",} };
        axios.post(`http://martianrobots-api.herokuapp.com/api/robotPositionsForGivenInstructions`, inputValue, headers)
            .then(res => {
                const outputValue = res.data;
                this.setState({ outputValue , invalid: false});
            }).catch(error => {
                console.log((JSON.stringify(error)));
                this.setState({outputValue: "Input is Invalid", invalid: true})
            });
    }

    render() {
        let placeholder = `Sample Input:
        5 3
        1 1 E
        RFRFRFRF
        3 2 N
        FRRFLLFFRRFLL
        0 3 W
        LLFFFLFLFL`;
        let {inputValue, outputValue, invalid} = this.state;
        return (
            <div className="container">
                <h4>Input:</h4>
                <form onSubmit={this.handleSubmit}>
                    <div className="mb-3">
                        <textarea className="form-control textarea" value={this.state.inputValue} onChange={this.handleChange}
                                  placeholder={placeholder}/>
                    </div>
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
                <div>
                    <h4 className="">Output: </h4>
                    <NewlineText text={outputValue} />
                </div>
            </div>

        );
    }
}

export default Home;