import React from 'react';

export default function NewlineText({text}) {
    const newText = text.split(/\n/g).map(str => <p>{str}</p>);

    return newText;
}