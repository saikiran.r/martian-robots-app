import Home from './components/Home';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
          Martian Robots Coding Challenge
      </header>
      <Home />
    </div>
  );
}

export default App;
